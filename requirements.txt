# This is a generated file; DO NOT EDIT!
#
# Please edit 'setup.cfg' to add top-level dependencies and use
# './contrib/refresh-requirements.sh to regenerate this file

amqp==5.1.1
asgiref==3.5.2
async-timeout==4.0.2
attrs==21.4.0
billiard==3.6.4.0
boto3==1.21.46
botocore==1.24.46
celery==5.2.7
certifi==2022.6.15
cffi==1.15.1
charset-normalizer==2.1.0
click==8.1.3
click-didyoumean==0.3.0
click-plugins==1.1.1
click-repl==0.2.0
contextlib2==21.6.0
cryptography==37.0.4
defusedxml==0.7.1
Deprecated==1.2.13
dj-database-url==0.5.0
Django==4.0.6
django-allauth==0.50.0
django-appconf==1.0.5
django-compressor==4.0
django-crispy-forms==1.14.0
django-csp==3.7
django-debug-toolbar==3.2.4
django-filter==21.1
django-shortuuidfield==0.1.3
django-storages==1.12.3
djangorestframework==3.13.1
dnspython==1.16.0
docopt==0.6.2
drf-spectacular==0.22.1
ecdsa==0.17.0
ephem==4.1.3
eventlet==0.30.2
graphviz==0.20
greenlet==1.1.2
gunicorn==19.9.0
hiredis==2.0.0
idna==3.3
importlib-metadata==4.12.0
inflection==0.5.1
internetarchive==3.0.2
jmespath==1.0.1
jsonpatch==1.32
jsonpointer==2.3
jsonschema==4.6.2
kombu==5.2.4
Markdown==3.3.7
mysqlclient==2.1.1
oauthlib==3.2.0
objgraph==3.5.0
packaging==21.3
Pillow==9.1.1
prompt-toolkit==3.0.30
pyasn1==0.4.8
pycparser==2.21
PyJWT==2.4.0
pyparsing==3.0.9
pyrsistent==0.18.1
python-dateutil==2.8.2
python-decouple==3.6
python-dotenv==0.20.0
python-jose==3.3.0
python3-openid==3.2.0
pytz==2022.1
PyYAML==6.0
rcssmin==1.1.0
redis==4.2.2
requests==2.28.1
requests-oauthlib==1.3.1
rjsmin==1.2.0
rsa==4.8
s3transfer==0.5.2
schema==0.7.5
sentry-sdk==1.5.12
shortuuid==1.0.9
six==1.16.0
social-auth-app-django==5.0.0
social-auth-core==4.3.0
sqlparse==0.4.2
tinytag==1.8.1
tqdm==4.64.0
uritemplate==4.1.1
urllib3==1.26.10
vine==5.0.0
wcwidth==0.2.5
wrapt==1.14.1
zipp==3.8.0
